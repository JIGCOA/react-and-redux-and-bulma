import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import 'bulma/css/bulma.css';
import './index.css';

const AppRouter = () => (
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
  
 ReactDOM.render(<AppRouter />, document.getElementById('root'));
 serviceWorker.unregister();
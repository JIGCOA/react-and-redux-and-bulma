import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Switch,Route } from 'react-router-dom';

import About from './pages/About.js';
import Signin from './pages/Signin.js';
import Signup from './pages/Signup.js';
import Home from './pages/Home.js';




class App extends Component {
    render() {
      return (
        //เมนูbulma 
        <div className="my-app">
          <nav className="navbar is-light" role="navigation" aria-label="main navigation">
            <div className="container">
              <div className="navbar-brand">
              <NavLink to="/" className="navbar-item">JIGCOASHOP</NavLink>

              </div>
              <div className="navbar-menu">
                <div className="navbar-end">
                  <NavLink exact to="/" activeClassName="is-active"  className="navbar-item">Home</NavLink>
                  <NavLink to="/about"  activeClassName="is-active"  className="navbar-item">About</NavLink>
                  <NavLink to="/signin" activeClassName="is-active"  className="navbar-item">Sing in</NavLink>
                  <NavLink to="/signup" activeClassName="is-active"  className="navbar-item">Sign up</NavLink>
                </div>
              </div>
            </div>
          </nav>
          <div className="App container">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about"  component={About}/>
            <Route exact path="/signin"  component={Signin}/>
            <Route exact path="/signup"  component={Signup}/>
          </Switch>
            
         
         </div>
        </div>
      )
    }
  }
export default App
